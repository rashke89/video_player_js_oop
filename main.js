var video = document.getElementById('video');
var play = document.getElementById('play');
var stop = document.getElementById('stop');
var btns = document.getElementsByClassName('buttons')[0];
console.log(btns);
btns.addEventListener('mouseenter', show);
btns.addEventListener('mouseleave', hide);
stop.addEventListener('click', reload)

function show() {
    play.style.display = 'block';
    stop.style.display = 'block';

}

function hide() {
    play.style.display = 'none';
    stop.style.display = 'none';
}
play.addEventListener('click', playPause);

function playPause() {
    if (play.getAttribute('src') == 'icon-play-128.png') {
        video.play();
        play.setAttribute('src', 'pause.png')
    } else {
        video.pause();
        play.setAttribute('src', 'icon-play-128.png')
    }
};

function reload() {
    video.load();
    play.setAttribute('src', 'pause.png');
    video.play();
}