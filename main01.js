//code encapsulation
var player = {
    video: document.getElementById('video'),
    play: document.getElementById('play'),
    stop: document.getElementById('stop'),
    btns: document.getElementsByClassName('buttons')[0],
    events: function() {
        this.btns.addEventListener('mouseenter', this.show);
        this.btns.addEventListener('mouseleave', this.hide);
        this.play.addEventListener('click', this.playPause);
        this.stop.addEventListener('click', this.reload)
    },
    show: function() {
        player.play.style.display = 'block';
        player.stop.style.display = 'block';
    },
    hide: function() {
        player.play.style.display = 'none';
        player.stop.style.display = 'none';
    },
    playPause: function() {
        if (player.play.getAttribute('src') == 'icon-play-128.png') {
            player.video.play();
            player.play.setAttribute('src', 'pause.png')
        } else {
            player.video.pause();
            player.play.setAttribute('src', 'icon-play-128.png')
        }
    },
    reload: function() {
        player.video.load();
        player.play.setAttribute('src', 'pause.png');
        player.video.play();
    }
};

player.events();