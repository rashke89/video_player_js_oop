//bind Arrow functions
function Player(id) {
    var self = this;
    // console.log(self)
    this.video = document.getElementById(id);
    this.btns = this.video.nextElementSibling;
    this.play = this.btns.children[0];
    this.stop = this.btns.children[1];
    this.events = function() {
        this.events = function() {
            this.btns.addEventListener('mouseenter', () => {
                this.play.style.display = 'block';
                this.stop.style.display = 'block';
            });
            this.btns.addEventListener('mouseleave', () => {
                this.play.style.display = 'none';
                this.stop.style.display = 'none';
            });
        };

        this.show = function() {
            this.play.style.display = 'block';
            this.stop.style.display = 'block';
        };
        this.hide = function() {
            this.play.style.display = 'none';
            this.stop.style.display = 'none';
        }
    }
}
var video1 = new Player('video').events();
var video2 = new Player('video2').events();
